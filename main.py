import glob

from converter import *
from encoder import *
from decoder import *

import os

if __name__ == "__main__":
    vid2img()

    directory = r'./data/'
    index = 0

    files = [f for f in os.listdir(directory) if isfile(join(directory, f))]  # for sorting the file names properly
    files.sort(key=lambda x: int(x[5:-4]))

    # file1 = open("uncompressed_texts", "w+")

    for filename in files:
        print('compress frame ' + str(index))
        # encode('./data/' + filename,index ,'./data/frame' + str(index-(index%5)))
        if (index != 0):
            encode('./data/' + filename, index, './uncompresseds/uncompressed' + str(index - 1) + '.bmp')
        else:
            encode('./data/' + filename, index, './data' + filename)
        index += 1

    decode()

    img2vid()
