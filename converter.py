import cv2
import os
from os.path import isfile, join
import numpy as np


def vid2img():
    # Read the video from specified path
    cam = cv2.VideoCapture("sample 30 frame 1 min.mp4")

    try:
        # creating a folder named data
        if not os.path.exists('data'):
            os.makedirs('data')
    except OSError:
        print('Error: Creating directory of data')

    currentframe = 0
    counter = 0

    while (True):
        # reading from frame
        ret, frame = cam.read()

        if ret:
            frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            frame = frame.astype(np.float32)
            if(counter%5 != 0 ):
                frame = np.subtract(prev_frame,frame)
            prev_frame = frame

            # if video is still left continue creating images
            name = './data/frame' + str(currentframe) + '.jpg'
            print('Creating...' + name)

            # writing the extracted images
            cv2.imwrite(name, frame)

            currentframe += 1
            counter += 1

        else:
            break

    # Release all space and windows once done
    cam.release()
    cv2.destroyAllWindows()


def img2vid():
    pathIn = './compresseds/'
    pathOut = 'video.avi'
    fps = 30
    frame_array = []
    files = [f for f in os.listdir(pathIn) if isfile(join(pathIn, f))]  # for sorting the file names properly
    files.sort(key=lambda x:int(x[16:-4]))

    counter = 0

    for i in range(len(files)):
        filename = pathIn + files[i]
        # reading each files
        img = cv2.imread(filename)
        # print(img)

        # if (counter % 5 != 0):
        #     img = np.add(prev_frame, img)
        # prev_frame = img

        height, width, layers = img.shape
        size = (width, height)

        # inserting the frames into an image array
        frame_array.append(img)
        out = cv2.VideoWriter(pathOut, cv2.VideoWriter_fourcc(*'DIVX'), fps, size)
        for i in range(len(frame_array)): # writing to a image array
            out.write(frame_array[i])
    out.release()










