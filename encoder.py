import cv2
import math
import os
from zigzag import *
import numpy as np


def get_run_length_encoding(image):
    i = 0
    skip = 0
    stream = []
    bitstream = ""
    image = image.astype(int)
    while i < image.shape[0]:
        if image[i] != 0:
            stream.append((image[i], skip))
            bitstream = bitstream + str(image[i]) + " " + str(skip) + " "
            skip = 0
        else:
            skip = skip + 1
        i = i + 1

    return bitstream


# defining block size
block_size = 8


#Quantization Matrix
QUANTIZATION_MAT = np.array(
    [[16, 11, 10, 16, 24, 40, 51, 61], [12, 12, 14, 19, 26, 58, 60, 55], [14, 13, 16, 24, 40, 57, 69, 56],
     [14, 17, 22, 29, 51, 87, 80, 62], [18, 22, 37, 56, 68, 109, 103, 77], [24, 35, 55, 64, 81, 104, 113, 92],
     [49, 64, 78, 87, 103, 121, 120, 101], [72, 92, 95, 98, 112, 100, 103, 99]])

def encode(image, index ,prev_padded_image):
    img = cv2.imread(image, cv2.IMREAD_GRAYSCALE)
    img = img.astype(np.float32)
    prev_padded_image = cv2.imread(prev_padded_image, cv2.IMREAD_GRAYSCALE)
    if(index !=0):
        prev_padded_image = prev_padded_image.astype(np.float32)

    # get size of the image
    [h, w] = img.shape

    height = h
    width = w

    #number of blocks in height/width
    nbh = math.ceil(np.float32(h) / block_size)
    nbh = np.int32(nbh)

    nbw = math.ceil(np.float32(w) / block_size)
    nbw = np.int32(nbw)

    # Pad the image, because sometime image size is not dividable to block size
    # height of padded image
    H = block_size * nbh

    # width of padded image
    W = block_size * nbw

    # create a numpy zero matrix with size of H,W
    padded_img = np.zeros((H, W))

    # copy the values of img into padded_img
    if (index%5 != 0):
        padded_img[0:height, 0:width] = np.add(img[0:height, 0:width] , prev_padded_image[0:height, 0:width])
        padded_img[padded_img > 255] = 255
        padded_img[padded_img < 0] = 0

    else:
        padded_img[0:height, 0:width] = img[0:height, 0:width]


    try:
        if not os.path.exists('uncompresseds'):
            os.makedirs('uncompresseds')
    except OSError:
        print('Error: Creating directory of uncompresseds')

    cv2.imwrite('./uncompresseds/uncompressed'+str(index)+'.bmp', np.uint8(padded_img))

    # encoding:
    # divide image into block size by block size (here: 8-by-8) blocks
    # To each block apply 2D discrete cosine transform
    # reorder DCT coefficients in zig-zag order
    # reshaped it back to block size by block size (here: 8-by-8)

    for i in range(nbh):
        # Compute start and end row index of the block
        row_ind_1 = i * block_size
        row_ind_2 = row_ind_1 + block_size

        for j in range(nbw):
            # Compute start & end column index of the block
            col_ind_1 = j * block_size
            col_ind_2 = col_ind_1 + block_size

            block = padded_img[row_ind_1: row_ind_2, col_ind_1: col_ind_2]

            # apply 2D discrete cosine transform to the selected block
            DCT = cv2.dct(block)

            DCT_normalized = np.divide(DCT, QUANTIZATION_MAT).astype(int)

            # reorder DCT coefficients in zig zag order. it will give us a one dimentional array
            reordered = zigzag(DCT_normalized)

            # reshape the reorderd array back to (block size by block size)
            reshaped = np.reshape(reordered, (block_size, block_size))

            # copy reshaped matrix into padded_img on current block corresponding indices
            padded_img[row_ind_1: row_ind_2, col_ind_1: col_ind_2] = reshaped


    # cv2.imshow('encoded image', np.uint8(padded_img))

    arranged = padded_img.flatten()

    bitstream = get_run_length_encoding(arranged)

    # Two terms are assigned for size as well, semicolon denotes end of image to reciever
    bitstream = str(padded_img.shape[0]) + " " + str(padded_img.shape[1]) + " " + bitstream + ";&"

    try:
        if not os.path.exists('uncompressed_texts'):
            os.makedirs('uncompressed_texts')
    except OSError:
        print('Error: Creating directory of uncompressed_texts')

    file1 = open("uncompressed_texts.txt", "a")
    file1.write(bitstream)
    file1.close()