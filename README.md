# video codec

we have 4 main files in project till now.
we have converter,
        encoder,
        zigzag,
        decoder
files.

in converter file we have 2 functions that convert video to image and a function that does the inverse of that, image to video.
in encoder file we perform computings on each image:
    performs DCT, 
    applies quantization (Q-Matrix taken is standard JPEG matrix obtained from psycho-visual) experiments)
    encodes it using Run Length Encoding.
in decoder file we decode each image file.

so basically what we need to do now are:
    1- create function that 
        gets a video, 
        converts video to images,
        encode each image,
        save encoded images,
        reads encoded images,
        decode them,
        converts decoded images to video.
    2- function to create quantization array from DCT matrix.
